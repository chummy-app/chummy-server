package exception;

import model.LogType;
import org.springframework.http.HttpStatus;

import java.util.List;

public class ParametersMultipleErrorsException extends GeneralException {

    private List<GeneralException> exceptions;

    public ParametersMultipleErrorsException(List<GeneralException> exceptions, LogType logType, HttpStatus httpStatus, String message) {
        super(httpStatus, message);
        super.log(this.getClass(), logType);
        this.exceptions = exceptions;
    }

    public List<GeneralException> getExceptions(){
        return this.exceptions;
    }

}
