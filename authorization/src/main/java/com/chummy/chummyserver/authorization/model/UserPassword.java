package com.chummy.chummyserver.authorization.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Table(name = "UserPassword")
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserPassword {

    @Id
    private UUID id;

    @Column(unique=true)
    private String email;

    @Column(length = 70)
    private String password;

    @OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    private List<Role> roles;

}
