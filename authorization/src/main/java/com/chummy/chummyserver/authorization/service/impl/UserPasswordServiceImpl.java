package com.chummy.chummyserver.authorization.service.impl;

import com.chummy.chummyserver.authorization.exception.MailNotCorrectException;
import com.chummy.chummyserver.authorization.exception.ParameterException;
import com.chummy.chummyserver.authorization.model.Role;
import com.chummy.chummyserver.authorization.model.UserPassword;
import com.chummy.chummyserver.authorization.repository.RoleRepository;
import com.chummy.chummyserver.authorization.repository.UserPasswordRepository;
import com.chummy.chummyserver.authorization.service.UserPasswordService;
import exception.GeneralException;
import exception.ParametersMultipleErrorsException;
import model.LogType;
import model.dto.UserAuthorizationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
public class UserPasswordServiceImpl implements UserPasswordService {

    private static final Logger logger = LoggerFactory.getLogger(UserPasswordServiceImpl.class);

    private static final String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";

    private final PasswordEncoder passwordEncoder;

    private final UserPasswordRepository userPasswordRepository;

    private final RoleRepository roleRepository;

    public UserPasswordServiceImpl(UserPasswordRepository userPasswordRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
        this.userPasswordRepository = userPasswordRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public UserPassword signup(UserAuthorizationDto userAuthorizationDto) throws ParametersMultipleErrorsException {

        List<GeneralException> exceptions = getErrors(userAuthorizationDto);

        if(exceptions.size() > 0){
            throw new ParametersMultipleErrorsException(exceptions, null, HttpStatus.BAD_REQUEST, "Parameters received are not correct");
        }

        Role roleNewUser = null;
        Optional<Role> roleUser = roleRepository.findByName("USER");
        if(roleUser.isPresent()){
            roleNewUser = roleUser.get();
        }

        UserPassword userPassword = UserPassword.builder()
                .id(userAuthorizationDto.getId())
                .email(userAuthorizationDto.getEmail())
                .password(passwordEncoder.encode(userAuthorizationDto.getPassword()))
                .roles(Collections.singletonList(roleNewUser))
                .build();

        userPasswordRepository.save(userPassword);

        logger.info("New user {} has been created", userPassword.getEmail());

        return userPassword;

    }

    private List<GeneralException> getErrors(UserAuthorizationDto userAuthorizationDto){
        List<GeneralException> errors = new ArrayList<>();

        if(userAuthorizationDto == null){
            errors.add(new ParameterException(null, HttpStatus.BAD_REQUEST, "Parameters received are not correct"));
        }else{
            if(userAuthorizationDto.getEmail().isEmpty() || !mailIsValid(userAuthorizationDto.getEmail())){
                errors.add(new MailNotCorrectException(null, HttpStatus.BAD_REQUEST, "The format of email you have entered is not correct"));
            }

            // Check condition
        }

        return errors;
    }

    private static boolean mailIsValid(String email){
        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

}
