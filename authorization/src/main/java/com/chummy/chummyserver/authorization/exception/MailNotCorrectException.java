package com.chummy.chummyserver.authorization.exception;

import exception.GeneralException;
import model.LogType;
import org.springframework.http.HttpStatus;

public class MailNotCorrectException extends GeneralException {

    public MailNotCorrectException(LogType logType, HttpStatus httpStatus, String message) {
        super(httpStatus, message);
        super.log(this.getClass(), logType);
    }

}
