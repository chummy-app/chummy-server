package com.chummy.chummyserver.authorization.exception;

import exception.GeneralException;
import model.LogType;
import org.springframework.http.HttpStatus;

public class ParameterException extends GeneralException {

    public ParameterException(LogType logType, HttpStatus httpStatus, String message) {
        super(httpStatus, message);
        super.log(this.getClass(), logType);
    }

}
