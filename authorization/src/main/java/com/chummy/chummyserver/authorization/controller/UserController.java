package com.chummy.chummyserver.authorization.controller;

import com.chummy.chummyserver.authorization.model.UserPassword;
import com.chummy.chummyserver.authorization.service.UserPasswordService;
import exception.ParametersMultipleErrorsException;
import model.dto.UserAuthorizationDto;
import model.view.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private final UserPasswordService userPasswordService;

    public UserController(UserPasswordService userPasswordService) {
        this.userPasswordService = userPasswordService;
    }

    @RequestMapping("/validateUser")
    public Principal user(Principal user) {
        return user;
    }

    @PostMapping(value = "/signup", produces = { "application/json" })
    public ResponseEntity<ResponseView> create(@RequestBody @Valid UserAuthorizationDto userAuthorizationDto) throws ParametersMultipleErrorsException {
        UserPassword userCreated = userPasswordService.signup(userAuthorizationDto);

        return ResponseEntity.ok().body(ResponseView.builder()
                .statusHttp(HttpStatus.CREATED.value())
                .messageHttp(HttpStatus.CREATED.getReasonPhrase())
                .data(userCreated)
                .build());
    }

}
