package com.chummy.chummyserver.authorization.repository;

import com.chummy.chummyserver.authorization.model.UserPassword;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserPasswordRepository extends CrudRepository<UserPassword, UUID> {

    Optional<UserPassword> findUserPasswordById(UUID id);

    Optional<UserPassword> findUserPasswordByEmail(String email);

}