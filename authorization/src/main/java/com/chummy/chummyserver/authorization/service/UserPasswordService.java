package com.chummy.chummyserver.authorization.service;

import com.chummy.chummyserver.authorization.model.UserPassword;
import exception.ParametersMultipleErrorsException;
import model.dto.UserAuthorizationDto;
import org.springframework.stereotype.Service;

@Service
public interface UserPasswordService {

    UserPassword signup(UserAuthorizationDto userAuthorizationDto) throws ParametersMultipleErrorsException;

}
