package com.chummy.chummyserver.authorization.configuration;

import com.chummy.chummyserver.authorization.model.Role;
import com.chummy.chummyserver.authorization.repository.RoleRepository;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Configuration
public class InitializeRole {

    private final static String nameRoleUser = "USER";

    private final RoleRepository roleRepository;

    public InitializeRole(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @PostConstruct
    public void init(){
        if (roleRepository.count() == 0) {
            Role roleUser = new Role();
            roleUser.setName(nameRoleUser);

            Role roleAdmin = new Role();
            roleAdmin.setName("ADMIN");

            roleRepository.saveAll(Arrays.asList(roleUser, roleAdmin));
        }
    }

}
