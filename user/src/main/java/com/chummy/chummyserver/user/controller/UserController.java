package com.chummy.chummyserver.user.controller;

import com.chummy.chummyserver.user.exception.EmailAlreadyExistsException;
import com.chummy.chummyserver.user.model.User;
import com.chummy.chummyserver.user.model.dto.UserDto;
import com.chummy.chummyserver.user.service.UserService;
import model.view.ResponseView;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/test")
    public String test() {
        return "Hello World";
    }

    @PostMapping(value = "/signup", produces = { "application/json" })
    public ResponseEntity<ResponseView> create(@RequestBody @Valid UserDto userDto) throws EmailAlreadyExistsException {
        User user = userService.create(userDto);

        return ResponseEntity.ok().body(ResponseView.builder()
                .statusHttp(HttpStatus.CREATED.value())
                .messageHttp(HttpStatus.CREATED.getReasonPhrase())
                .data(user)
                .build());
    }

}