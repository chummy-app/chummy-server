package com.chummy.chummyserver.user.exception;

import exception.GeneralException;
import model.LogType;
import org.springframework.http.HttpStatus;

public class PasswordSaveException extends GeneralException {

    public PasswordSaveException(LogType logType, HttpStatus httpStatus, String message) {
        super(httpStatus, message);
        super.log(this.getClass(), logType);
    }

}
