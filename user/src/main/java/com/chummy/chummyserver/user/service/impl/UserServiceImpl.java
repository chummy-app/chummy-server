package com.chummy.chummyserver.user.service.impl;

import com.chummy.chummyserver.user.exception.EmailAlreadyExistsException;
import model.LogType;
import com.chummy.chummyserver.user.model.User;
import com.chummy.chummyserver.user.model.dto.UserDto;
import com.chummy.chummyserver.user.repository.UserRepository;
import com.chummy.chummyserver.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(UserDto userDto) throws EmailAlreadyExistsException {

        if(userRepository.findByEmail(userDto.getEmail()) != null){
            throw new EmailAlreadyExistsException(LogType.WARN, HttpStatus.BAD_REQUEST, "The email address you've entered is already in use");
        }

        Timestamp now = new Timestamp(new Date().getTime());

        User user = User.builder()
                .email(userDto.getEmail())
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .phoneNumber(userDto.getPhoneNumber())
                .isActivated(null)
                .creationDate(now)
                .updateDate(now)
                .lastConnection(null)
                .build();

        //Password save - Authorization microservices

        userRepository.save(user);

        logger.info("New user has been created : {}", user.getEmail());

        return user;

    }

}
