package com.chummy.chummyserver.user.exception;

import exception.GeneralException;
import model.LogType;
import org.springframework.http.HttpStatus;

public class EmailAlreadyExistsException extends GeneralException {

    public EmailAlreadyExistsException(LogType logType, HttpStatus httpStatus, String message) {
        super(httpStatus, message);
        super.log(this.getClass(), logType);
    }

}
