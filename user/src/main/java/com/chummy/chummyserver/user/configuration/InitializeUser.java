package com.chummy.chummyserver.user.configuration;

import com.chummy.chummyserver.user.exception.EmailAlreadyExistsException;
import com.chummy.chummyserver.user.model.dto.UserDto;
import com.chummy.chummyserver.user.service.UserService;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class InitializeUser {

    private final static String userEmail = "test@test.fr";

    private final UserService userService;

    public InitializeUser(UserService userService) {
        this.userService = userService;
    }

    @PostConstruct
    public void init(){
        UserDto userDto = UserDto.builder()
            .firstName("test")
            .lastName("test")
            .email(userEmail)
            .password("test1234")
            .phoneNumber("0600000000")
            .build();

        try {
            userService.create(userDto);
        } catch (EmailAlreadyExistsException ignored) {}
    }

}
