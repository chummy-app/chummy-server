package com.chummy.chummyserver.user.service;

import com.chummy.chummyserver.user.exception.EmailAlreadyExistsException;
import com.chummy.chummyserver.user.model.User;
import com.chummy.chummyserver.user.model.dto.UserDto;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    User create(UserDto userDto) throws EmailAlreadyExistsException;

}
